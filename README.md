# Textpattern one-click updater #
 
An easy process to update a TXP's installation like [WP's update-core](http://codex.wordpress.org/Updating_WordPress) does. As in WP update process:

- file ownership: all of your TXP files must be owned by the user under which your web server executes. In other words, the owner of your WordPress files must match the user under which your web server executes. The web server user (named "apache", "web", "www", "nobody", or some such) is not necessarily the owner of your WordPress files. Typically, TXP files are owned by the ftp user which uploaded the original files. If there is no match between the owner of your WordPress files and the user under which your web server executes, you will receive a dialog box asking for "connection information", and you will find that no matter what you enter in that dialog box, you won't be able to update using the "Update Now" button.
- file permissions: all of your WordPress files must be either owner writable by, or group writable by, the user under which your Apache server executes.




Tested on Windows, not in Linux
